<?php

    //function to connect us to the database
    function connect() {
        $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);

        if($conn->connect_errno > 0) {
            die('Unable to connect to database [' . $conn->connect_error . ']');
        }

        return $conn;
    }

    function get_all_data()
{
    //Step 01:
    //Prepare Variables
    
    $db = connection();
    $sql = "SELECT * FROM tbl_students"; //change the table to match
    $arr = []; //Create an array for the data
    
    //Run Query
    $result = $db->query($sql); //The result of the query is stored in a new variable
    
    //Error checking
    if(!$result)
    {
        die('There was an error with your query [' .$db->error . ']');
    }
    
    //Step 02:
    //Loop through the received data
    
    //In the array add ALL the the columns that are being requested with the query
    //The last part does not require a semi column
    //The reference in the [ & ] needs to match the case that is in the actual database
    
    while($row = $result->fetch_assoc())
    {
        $arr = array (
            'id' =>  $row['ID'],
            'fname' =>  $row['FNAME']
        );
    }
    
    //Check the data in your array
    //var_dump($arr);
    
    //Export your data into a JSON string
    $json = json_encode(array("data" => $arr));
    
    //Check the data as a json string
    //print_r($_json);
    
    //Step 03:
    //Cleaning up and closing connections - make you sure you export your data before you close it.
    
    $result->free();
    $db->close();
}

?>