<?php include_once('functions/constants.php'); ?>
<?php /*
    $id = $_GET['id'];
    
    $array = json_decode(loadData($id), True);

    $fname = $array[0][0]['fname'];
    $lname = $array[0][0]['lname'];
    $dob = $array[0][0]['dob'];

    $dob2 = date("Y-m-d", strtotime($dob));
*/?>

<!doctype html>
<html>
    <head>
        <title>People</title>
        <link rel="stylesheet" href="css/main.css" type="text/css">
    </head>
    <body>
        <div class="container">
            <h1>People</h1>
            <h2 class="left">Edit Record</h2>
            <header>
            Edit Person = <?php echo $fname." ".$lname; ?>
            </header>
            <form method="POST" >
                <input type="hidden" name="id" value="<?php echo $id; ?>">
                <input type="text" name="fname" placeholder="first name" value="<?php echo $fname; ?>">
                <input type="text" name="lname" placeholder="last name" value="<?php echo $lname; ?>">
                <input type="date" name="dob" placeholder="dd-mm-yyyy" value="<?php echo $dob2; ?>">
                <input type="submit" value="submit form">
            </form>
            <h2 class="left"><a href="admin.php">Cancel</a></h2>
        </div>
    </body>
</html>